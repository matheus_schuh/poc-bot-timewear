package com.iwsbrazil.futurefaces.pocbottimewear;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.JsonElement;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ai.api.PartialResultsListener;
import ai.api.android.AIConfiguration;
import ai.api.model.AIError;
import ai.api.model.AIResponse;
import ai.api.model.Metadata;
import ai.api.model.Result;
import ai.api.model.Status;
import ai.api.ui.AIButton;

public class MainActivity extends BaseActivity
        implements AIButton.AIButtonListener, PartialResultsListener, MaterialSpinner.OnItemSelectedListener {


    private static final String TAG = MainActivity.class.getName();

    private AIButton aiButton;
    private TextView resultTextView;
    private TextView partialResultsTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TTS.init(getApplicationContext());

        resultTextView = (TextView) findViewById(R.id.resultTextView);
        partialResultsTextView = (TextView) findViewById(R.id.partialResultsTextView);
        aiButton = (AIButton) findViewById(R.id.micButton);

        MaterialSpinner spinner = (MaterialSpinner) findViewById(R.id.selectLanguageSpinner);
        spinner.setItems(Config.languages);
        spinner.setOnItemSelectedListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkAudioRecordPermission();
    }

    @Override
    protected void onPause() {
        super.onPause();

        // use this method to disconnect from speech recognition service
        // Not destroying the SpeechRecognition object in onPause method would block other apps from using SpeechRecognition service
        aiButton.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // use this method to reinit connection to recognition service
        aiButton.resume();
    }

//    @Override
//    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//        final LanguageConfig selectedLanguage = (LanguageConfig) parent.getItemAtPosition(position);
//        updateButtonConfig(selectedLanguage);
//        TTS.setLanguage(selectedLanguage);
//    }
//
//    @Override
//    public void onNothingSelected(AdapterView<?> adapterView) {
//
//    }

    private void updateButtonConfig(LanguageConfig selectedLanguage) {
        final AIConfiguration.SupportedLanguages lang = AIConfiguration.SupportedLanguages.fromLanguageTag(selectedLanguage.getLanguageCode());
        final AIConfiguration config = new AIConfiguration(selectedLanguage.getAccessToken(),
                lang,
                AIConfiguration.RecognitionEngine.System);

        aiButton.initialize(config);
        aiButton.setResultsListener(this);
        aiButton.setPartialResultsListener(this);
    }

    @Override
    public void onResult(AIResponse response) {
        Log.d(TAG, "onResult");

        partialResultsTextView.setText("");

        Log.i(TAG, "Received success response");

        // this is example how to get different parts of result object
        final Status status = response.getStatus();
        Log.i(TAG, "Status code: " + status.getCode());
        Log.i(TAG, "Status type: " + status.getErrorType());

        final Result result = response.getResult();
        Log.i(TAG, "Resolved query: " + result.getResolvedQuery());

        Log.i(TAG, "Action: " + result.getAction());
        final String speech = result.getFulfillment().getSpeech();
        Log.i(TAG, "Speech: " + speech);
        TTS.speak(speech);
        resultTextView.setText(speech);

        final Metadata metadata = result.getMetadata();
        if (metadata != null) {
            Log.i(TAG, "Intent id: " + metadata.getIntentId());
            Log.i(TAG, "Intent name: " + metadata.getIntentName());
        }

        final HashMap<String, JsonElement> params = result.getParameters();
        if (params != null && !params.isEmpty()) {
            Log.i(TAG, "Parameters: ");
            for (final Map.Entry<String, JsonElement> entry : params.entrySet()) {
                Log.i(TAG, String.format("%s: %s", entry.getKey(), entry.getValue().toString()));
            }
        }
    }

    @Override
    public void onError(final AIError error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "onError");
                resultTextView.setText(error.toString());
            }
        });
    }

    @Override
    public void onCancelled() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "onCancelled");
                resultTextView.setText("");
            }
        });
    }

    @Override
    public void onPartialResults(List<String> partialResults) {
        partialResultsTextView.setText(partialResults.get(0));

    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
        final LanguageConfig selectedLanguage = (LanguageConfig) item;
        updateButtonConfig(selectedLanguage);
        TTS.setLanguage(selectedLanguage);

    }
}
