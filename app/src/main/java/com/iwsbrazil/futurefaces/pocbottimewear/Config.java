package com.iwsbrazil.futurefaces.pocbottimewear;

public abstract class Config {
    // copy this keys from your developer dashboard
    public static final String ACCESS_TOKEN = "3485a96fb27744db83e78b8c4bc9e7b7";

    public static final LanguageConfig[] languages = new LanguageConfig[]{
            new LanguageConfig("en", "cfc16e3b12fe4285b7f9682f57b77036"),
            new LanguageConfig("pt-BR", "fc32ed6f13ec43e596def89a734aac36"),
    };

    public static final String[] events = new String[]{
            "hello_event",
            "goodbye_event",
            "how_are_you_event"
    };
}